// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlQuery>

class Database : public QObject
{
    Q_OBJECT
public:
    static Database &instance()
    {
        static Database _instance;
        return _instance;
    }
    Q_INVOKABLE QString path();
    bool init();
    bool execute(QSqlQuery &query);
    bool execute(const QString &query);

    Q_INVOKABLE void setSeen(const QString &bug, bool seen);
    bool updateTemplate(const QString &id, const QString &title, const QString &content);
    Q_INVOKABLE void addTemplate( const QString &title, const QString &content);
    Q_INVOKABLE void deleteTemplate(const QString &id);


Q_SIGNALS:
    void bugSeenChanged(const QString &entryId, bool read);
    void templatesChanged();


private:
    Database();

};

#endif // DATABASE_H
