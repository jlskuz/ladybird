// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.19 as Kirigami

import org.kde.ladybird 1.0 as Ladybird

Kirigami.AbstractCard {
    id: massageBox
    property string author
    property bool isSelf: false
    property date creationDateTime
    property string message
    property int attachmentId: 0

    width: parent.width

    contentItem: ColumnLayout {
        id: column
        spacing: Kirigami.Units.smallSpacing
        Flow {
            id: flowLayout
            Layout.fillWidth: true
            spacing: Kirigami.Units.smallSpacing
            visible: true //model.showAuthor
            Kirigami.Icon {
                source: "help-hint"
                width: height
                Layout.preferredWidth: Kirigami.Units.iconSizes.small
                Layout.preferredHeight: Kirigami.Units.iconSizes.small
                visible: author === "bug-janitor@kde.org"
            }
            QQC2.Label {
                id: nameLabel
                text: author
                textFormat: Text.PlainText
                font.weight: Font.Bold
                elide: Text.ElideMiddle
            }
            QQC2.Label {
                id: timeLabel

                text: creationDateTime.toLocaleString(Locale.ShortFormat)
                color: Kirigami.Theme.disabledTextColor
                QQC2.ToolTip.visible: hoverHandler.hovered
                QQC2.ToolTip.text: creationDateTime.toLocaleString(Locale.LongFormat)
                QQC2.ToolTip.delay: Kirigami.Units.toolTipDelay

                HoverHandler {
                    id: hoverHandler
                }
            }
        }
        Text {
            Layout.fillWidth: true
            Layout.fillHeight: true
            text: message
            wrapMode: Text.WordWrap
            textFormat: Text.MarkdownText
            onLinkActivated: Qt.openUrlExternally(link)
            color: Kirigami.Theme.textColor
        }
        QQC2.Button {
            visible: attachmentId !== 0
            //Layout.alignment: Qt.AlignRight
            icon.name: "mail-attachment-symbolic"
            text: i18n("Open attachment")
            onClicked: Qt.openUrlExternally("https://bugsfiles.kde.org/attachment.cgi?id=" + attachmentId)
        }
    }

    background: Item {
        Kirigami.ShadowedRectangle {
            id: bubbleBackground
            visible: true
            anchors.fill: parent
            Kirigami.Theme.colorSet: Kirigami.Theme.View
            color: {
                if (author === "bug-janitor@kde.org") {
                    return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, Kirigami.Theme.highlightColor, 0.15)
                } else if (isSelf) {
                    return Kirigami.Theme.positiveBackgroundColor
                } else {
                    return Kirigami.Theme.backgroundColor
                }
            }
            radius: Kirigami.Units.smallSpacing
            shadow.size: Kirigami.Units.smallSpacing
            shadow.color: attachmentId.attachmentId !== 0 ? Qt.rgba(0.0, 0.0, 0.0, 0.10) : Qt.rgba(Kirigami.Theme.textColor.r, Kirigami.Theme.textColor.g, Kirigami.Theme.textColor.b, 0.10)
            border.color: Kirigami.ColorUtils.tintWithAlpha(color, Kirigami.Theme.textColor, 0.15)
            border.width: 1
        }
    }

}
