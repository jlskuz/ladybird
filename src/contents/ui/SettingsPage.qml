// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.12 as Kirigami
import org.kde.ladybird 1.0 as Ladybird

Kirigami.ScrollablePage {
    title: i18n("Settings")

    Kirigami.FormLayout {
        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Bugzilla")
        }

        Controls.TextField {
            Kirigami.FormData.label: i18n("Base Url:")
            id: baseUrl
            text: Ladybird.Config.baseUrl
            onTextChanged: Ladybird.Config.baseUrl = text
        }

        Controls.TextField {
            Kirigami.FormData.label: i18n("Product:")
            id: bugProduct
            text: Ladybird.Config.product
            onTextChanged: Ladybird.Config.product = text
        }

        Controls.TextField {
            Kirigami.FormData.label: i18n("Api Key:")
            id: apiKey
            text: Ladybird.Config.apiKey
            onTextChanged: Ladybird.Config.apiKey = text
        }

        Controls.TextField {
            Kirigami.FormData.label: i18n("E-Mail:")
            id: userEmail
            text: Ladybird.Config.userEmail
            onTextChanged: Ladybird.Config.userEmail = text
        }

        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Debug Information")
        }

        Controls.Label {
            Kirigami.FormData.label: i18n("Database path:")
            id: dbPath
            text: Ladybird.Database.path() + "/database.db3"
        }
    }
}
