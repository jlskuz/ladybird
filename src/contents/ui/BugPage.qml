// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.19 as Kirigami

import org.kde.ladybird 1.0 as Ladybird

Kirigami.ScrollablePage {
    id: page

    property QtObject bug

    function statusIcon (name) {
        switch (name.toLowerCase()) {
            case "confirmed":
                return "emblem-mounted"
            case "resolved":
                return "emblem-success"
            case "needsinfo":
                return "emblem-pause"
            case "reopend":
                return "emblem-encrypted-unlocked"
            default:
                return "emblem-question"
        }
    }

    function severityIcon (name) {
        switch (name.toLowerCase()) {
            case "wishlist":
                return "tools-wizard"
            case "crash":
                return "tools-report-bug"
            case "task":
                return "task-reminder"
            case "normal":
                return "data-information"
            case "critical":
                return "data-error"
            case "grave":
                return "data-warning"
            case "minor":
                return "arrow-down"
            default:
                return ""
        }
    }

    function unspecifiedCheck (value) {
        if (value.toLowerCase() === "other") {
            return true
        }
        if (value === "unspecified") {
            return true
        }
        return false
    }

    title: i18n("Bug %1", bug.id)

    Component.onCompleted: bug.fetchComments()

    header: QQC2.Pane {
        Layout.fillWidth: true
        ColumnLayout {
            width: parent.width
            Kirigami.Heading {
                text: bug.summary
                wrapMode: Text.WordWrap
                Layout.fillWidth: true
            }
            Flow {
                Layout.fillWidth: true
                spacing: 10
                Kirigami.Chip {
                    text: bug.status
                    icon.name: statusIcon(bug.status)
                    closable: false
                    checkable: false
                }
                Kirigami.Chip {
                    text: bug.severity
                    icon.name: severityIcon(bug.severity)
                    closable: false
                }
                Kirigami.Chip {
                    text: bug.os
                    icon.name: unspecifiedCheck(bug.os) ? "data-warning" : "drive-harddisk"
                    closable: false
                }
                Kirigami.Chip {
                    text: bug.platform
                    icon.name: unspecifiedCheck(bug.platform) ? "data-warning" : ""
                    closable: false
                }

                Kirigami.Chip {
                    text: bug.version
                    icon.name: unspecifiedCheck(bug.version) ? "data-warning" : ""
                    closable: false
                }
            }
        }

    }

    Kirigami.LoadingPlaceholder {
        id: loadingPlaceholder
        anchors.centerIn: parent
        visible: commentList.count === 0 && bug.errorString === ""
    }
    Kirigami.PlaceholderMessage {
        visible: commentList.count === 0 && bug.errorString !== ""

        width: Kirigami.Units.gridUnit * 20
        anchors.centerIn: parent

        text: i18n("Error: %1", bug.errorString)
        icon.name: "data-error"
    }

    Kirigami.CardsListView {
        id: commentList
        model: bug.comments
        Layout.fillWidth: true

        delegate: ChatMessage {
            author: modelData.creator
            isSelf: modelData.creator === Ladybird.Config.userEmail
            creationDateTime: modelData.creationTime
            message: modelData.text
            attachmentId: modelData.attachmentId
        }

    }

    Connections {
        target: bug
        function onPostCommentDone(errorString) {
            if (errorString.lenght === 0) {
                commentMessage.errorString = ""
                messageField.clear()
                bug.commentDraft = ""
                dupId.clear()
                statusCombo.currentIndex = 0
                bug.statusDraft = 0
            } else {
                commentMessage.errorString = errorString
            }

            messageField.enabled = true
            dupId.enabled = true
            statusCombo.enabled = true
            console.log("Data: " + errorString)
            textToChange.text = "Changed to: " + signal_param
        }
    }

    footer: QQC2.Pane {
        id: pane
        Layout.fillWidth: true

        ColumnLayout {
            width: parent.width
            Kirigami.Separator { Layout.fillWidth: true }
            Kirigami.InlineMessage {
                id: commentMessage
                property string errorString: ""
                Layout.fillWidth: true
                Layout.leftMargin: 1 // So we can see the border
                Layout.rightMargin: 1 // So we can see the border
                type: errorString.length > 0 ? Kirigami.MessageType.Warning : Kirigami.MessageType.Information
                text: errorString.length > 0 ? errorString : (Ladybird.Config.apiKey.length === 0 ? i18n("To be able to post comments you need to specify a valid api key in the settings.") : "")
                visible: Ladybird.Config.apiKey.length === 0 || errorString.length > 0
            }
            RowLayout {
                visible: Ladybird.Config.apiKey.length > 0
                width: parent.width

                QQC2.TextArea {
                    id: messageField
                    text: bug.commentDraft
                    Layout.fillWidth: true
                    placeholderText: i18n("Write a comment")
                    wrapMode: QQC2.TextArea.Wrap
                    onTextChanged: bug.commentDraft = text
                }

                QQC2.Menu {
                    id: templatesMenu
                    title: i18n("Templates")
                    Repeater {
                        model: templatesModel
                        delegate: QQC2.MenuItem {
                            text: model.template.title
                            onTriggered: messageField.append(model.template.expandedContent(bug))
                        }
                    }
                }

                QQC2.Button {
                    onClicked: templatesMenu.popup()
                    icon.name: "insert-text-symbolic"
                    icon.width: Kirigami.Settings.isMobile ? Kirigami.Units.iconSizes.medium : Kirigami.Units.iconSizes.sizeForLabels
                    icon.height: Kirigami.Settings.isMobile ? Kirigami.Units.iconSizes.medium : Kirigami.Units.iconSizes.sizeForLabels

                }
            }
            RowLayout {
                width: parent.width
                Layout.alignment: Qt.AlignRight
                visible: Ladybird.Config.apiKey.length > 0

                QQC2.ComboBox {
                    id: statusCombo
                    currentIndex: bug.statusDraft
                    model: ListModel {
                        ListElement { text: "(No change)"; data: 0 }
                        ListElement { text: "Needs Info"; data: 1 }
                        ListElement { text: "Fixed"; data: 2 }
                        ListElement { text: "Unmaintained"; data: 3 }
                        ListElement { text: "Works for me"; data: 4 }
                        ListElement { text: "Confirmed"; data: 5 }
                        ListElement { text: "Reported"; data: 6 }
                        ListElement { text: "Not a Bug"; data: 7 }
                        ListElement { text: "Duplicate of"; data: 8 }
                    }
                    textRole: "text"
                    valueRole: "data"
                    Layout.fillWidth: true
                    onCurrentIndexChanged: bug.statusDraft = currentIndex
                }
                QQC2.TextField {
                    id: dupId
                    visible: statusCombo.currentValue === 8
                    placeholderText: i18n("ID of the bug this is a duplicate of")
                }

                QQC2.Button {
                    id: sendButton
                    text: i18n("Send")
                    icon.name: "document-send"
                    enabled: messageField.length > 0 || (statusCombo.currentValue === 8 && dupId.length > 0)
                    onClicked: {
                        bug.postComment(messageField.text, statusCombo.currentValue, dupId.visible ? dupId.text : "")
                        messageField.enabled = false
                        dupId.enabled = false
                        statusCombo.enabled = false
                    }
                }
            }
            /*Kirigami.Separator { Layout.fillWidth: true }

            Kirigami.NavigationTabBar {
                Layout.fillWidth: true
                shadow: false
                actions: [
                    Kirigami.Action {
                        iconName: "go-previous"
                        text: i18n("Previous")
                        onTriggered: {
                            // TODO
                        }
                    },
                    Kirigami.Action {
                        iconName: "go-next"
                        text: i18n("Next")
                        onTriggered: // TODO
                    }
                ]
            }*/
        }
    }

    actions {
        contextualActions: [
            Kirigami.Action {
                text: bug.seen ? i18n("Mark as unseen") : i18n("Mark as seen")
                icon.name: bug.seen ? "mail-mark-unread" : "mail-mark-read"
                onTriggered: bug.seen = !bug.seen
            },
            Kirigami.Action {
                text: i18n("Reload Comments")
                icon.name: "view-refresh"
                checkable: false
                onTriggered: bug.fetchComments(true)
            },
            Kirigami.Action {
                text: i18n("Copy Bug ID")
                icon.name: "edit-copy"
                checkable: false
                onTriggered: bug.copyId()
            },
            Kirigami.Action {
                        text: i18n("Open in Browser")
                        icon.name: "globe"
                        onTriggered: Qt.openUrlExternally("https://bugs.kde.org/" + bug.id)
                    }
        ]
    }
}
