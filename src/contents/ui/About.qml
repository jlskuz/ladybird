// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm
import org.kde.ladybird 1.0

MobileForm.AboutPage {
    aboutData: About
}
