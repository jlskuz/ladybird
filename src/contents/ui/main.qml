
// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.ladybird 1.0

Kirigami.ApplicationWindow {
    id: root

    title: "Ladybird"

    property bool bugListRefreshing: false
    property int bugCount: 0

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    Component.onCompleted: App.restoreWindowGeometry(root)

    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    globalDrawer: LadybirdGlobalDrawer {}

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: bugListComponent

    BugModel {
        id: bugsModel
        onRefreshingChanged: {
            if (!refreshing) {
                bugListRefreshing = false
            }
        }
        onRowsInserted: bugCount = rowCount()
        onRowsRemoved: bugCount = rowCount()
    }

    BugsProxyModel {
        id: proxyBugsModel
        seenFilter: 0
        severityFilter: ""
        statusFilter: ""
        searchFilter: ""
        sourceModel: bugsModel
    }

    TemplatesModel {
        id: templatesModel
    }

    Component {
        id: bugListComponent
        BugListPage {}
    }

    Component {
        id: templateComponent
        TemplateListPage {}
    }
}
