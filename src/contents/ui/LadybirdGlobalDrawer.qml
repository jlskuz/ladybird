// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import org.kde.kirigami 2.12 as Kirigami
import org.kde.ladybird 1.0

Kirigami.GlobalDrawer {
    id: root
    title: i18n("ladybird")
    titleIcon: "applications-graphics"
    isMenu: !root.isMobile
    actions: [
        Kirigami.Action {
            text: i18n("Bugs")
            icon.name: "view-list-details"
            onTriggered: {
                pageStack.clear()
                pageStack.push(bugListComponent)
            }
        },
        Kirigami.Action {
            text: i18n("Templates")
            icon.name: "insert-text-symbolic"
            onTriggered: {
                pageStack.clear()
                pageStack.push(templateComponent)
            }
        },
        Kirigami.Action {
            text: i18n("Settings")
            iconName: "settings-configure"
            onTriggered: {
                pageStack.clear()
                pageStack.push("qrc:/SettingsPage.qml")
            }
            enabled: pageStack.currentItem.title !== i18n("Settings")
        },
        Kirigami.Action {
            text: i18n("About Ladybird")
            icon.name: "help-about"
            onTriggered: pageStack.layers.push('qrc:About.qml')
        },
        Kirigami.Action {
            text: i18n("Quit")
            icon.name: "application-exit"
            onTriggered: Qt.quit()
        }
    ]
}
