// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include "comment.h"

#include <QJsonValue>

Comment::Comment(const QJsonObject &json)
    : QObject(nullptr)
    , m_id(QString::number(json[QStringLiteral("id")].toInt()))
    , m_creationTime(QDateTime::fromString(json[QStringLiteral("creation_time")].toString(), Qt::ISODate))
    , m_creator(json[QStringLiteral("creator")].toString())
    , m_text(json[QStringLiteral("text")].toString())
    , m_attachmentId(json[QStringLiteral("attachment_id")].toInt())
{
}
