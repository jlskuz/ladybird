// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include "bugsproxymodel.h"
#include "bug.h"
#include "database.h"
#include <KLocalizedString>

BugsProxyModel::BugsProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_seenFilter(BugsSeenFilter::ALL)
{
    connect(&Database::instance(), &Database::bugSeenChanged, this, [this]() {
        invalidateFilter();
    });
}

BugsProxyModel::~BugsProxyModel()
{}

void BugsProxyModel::setFlagsFilter(const QString &filter)
{
    if (m_flagsFilter != filter) {
        m_flagsFilter = filter;
        invalidateFilter();
        Q_EMIT flagsFilterChanged();
    }
}

void BugsProxyModel::setFlagsOperator(int value)
{
    if (m_flagsOperator != value) {
        m_flagsOperator = value;
        invalidateFilter();
        Q_EMIT flagsOperatorChanged();
    }
}

void BugsProxyModel::setOsFilter(const QString &filter)
{
    if (m_osFilter != filter) {
        m_osFilter = filter;
        invalidateFilter();
        Q_EMIT osFilterChanged();
    }
}

void BugsProxyModel::setSeenFilter(int filter)
{
    if (m_seenFilter != filter) {
        m_seenFilter = BugsSeenFilter(filter);
        invalidateFilter();
        Q_EMIT seenFilterChanged();
    }
}

void BugsProxyModel::setSeverityFilter(const QString &filter)
{
    if (m_severityFilter != filter) {
        m_severityFilter = filter;
        invalidateFilter();
        Q_EMIT severityFilterChanged();
    }
}

void BugsProxyModel::setStatusFilter(const QString &filter)
{
    if (m_statusFilter != filter) {
        m_statusFilter = filter;
        invalidateFilter();
        Q_EMIT statusFilterChanged();
    }
}

void BugsProxyModel::setSearchFilter(const QString &filter)
{
    if (m_searchFilter != filter) {
        m_searchFilter = filter;
        invalidateFilter();
        Q_EMIT searchFilterChanged();
    }
}

void BugsProxyModel::setVersionFilter(const QString &filter)
{
    if (m_versionFilter != filter) {
        m_versionFilter = filter;
        invalidateFilter();
        Q_EMIT versionFilterChanged();
    }
}

bool BugsProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    const auto idx = sourceModel()->index(source_row, 0, source_parent);

    if (!QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent)) {
        return false;
    }

    auto bug = idx.data(0).value<Bug *>();

    if (!m_flagsFilter.isEmpty() && m_flagsFilter != i18n("All")) {
        if (m_flagsOperator == 0 && !bug->flags().contains(m_flagsFilter)) {
            return false;
        } else if (m_flagsOperator == 1 && bug->flags().contains(m_flagsFilter)) {
            return false;
        }
    }

    if (!m_osFilter.isEmpty() && bug->os().toLower() != m_osFilter.toLower()) {
        return false;
    }

    if (m_seenFilter == BugsSeenFilter::UNSEEN && bug->seen()) {
        return false;
    }

    if (m_seenFilter == BugsSeenFilter::SEEN && !bug->seen()) {
        return false;
    }

    if (!m_severityFilter.isEmpty() && bug->severity().toLower() != m_severityFilter.toLower()) {
        return false;
    }

    if (!m_statusFilter.isEmpty() && bug->status().toLower() != m_statusFilter.toLower()) {
        return false;
    }

    if (!m_searchFilter.isEmpty() && !(bug->id().toLower().contains(m_searchFilter.toLower()) || bug->summary().toLower().contains(m_searchFilter.toLower()))) {
        return false;
    }

    if (!m_versionFilter.isEmpty() && !bug->version().toLower().contains(m_versionFilter.toLower())) {
        return false;
    }

    return true;
}
