// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QString>

#include "templatesmodel.h"

#include "database.h"

TemplatesModel::TemplatesModel(QObject *parent)
    : QAbstractListModel(parent)
{
    loadFromDatabase();
    connect(&Database::instance(), &Database::templatesChanged, this, [this]() {
        loadFromDatabase();
    });
}

/*TemplatesModel::~TemplatesModel()
{
    qDeleteAll(m_templates);
}*/

int TemplatesModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_templates.count();
}

QVariant TemplatesModel::data(const QModelIndex & index, int role) const {

    if (role != 0 ||  index.row() >= m_templates.length()) {
        return QVariant();
    }

    return QVariant::fromValue(m_templates[index.row()]);
}

QHash<int, QByteArray> TemplatesModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[0] = "template";
    return roles;
}

void TemplatesModel::loadFromDatabase()
{
    beginResetModel();

    qDeleteAll(m_templates);
    m_templates.clear();
    QSqlQuery q;
    q.prepare(QStringLiteral("SELECT * FROM Templates;"));
    Database::instance().execute(q);
    while (q.next()) {
        Template *item = new Template(q.value(QStringLiteral("id")).toString(), q.value(QStringLiteral("title")).toString(), q.value(QStringLiteral("content")).toString());
        m_templates << item;
    }

    endResetModel();
}

QJsonArray TemplatesModel::toJson()
{
    QJsonArray array;

    for (auto item : m_templates) {
        QJsonObject obj;
        obj[QStringLiteral("title")] = item->title();
        obj[QStringLiteral("content")] = item->content();
        array.append(obj);
    }

    return array;
}

void TemplatesModel::exportToFile(const QString &path)
{
    QUrl url(path);
    QFile file(url.isLocalFile() ? url.toLocalFile() : url.toString());
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QJsonDocument doc(toJson());
    file.write(doc.toJson(QJsonDocument::Indented));
    file.close();
}

void TemplatesModel::importFromFile(const QString &path)
{
    QUrl url(path);
    QFile file(url.isLocalFile() ? url.toLocalFile() : url.toString());
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    QString val = QString::fromLatin1(file.readAll());
    file.close();

    QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
    importJson(d.array());
}

void TemplatesModel::importJson(QJsonArray array)
{
    for (auto item : array) {
        QString title(item.toObject()[QStringLiteral("title")].toString());
        QString content(item.toObject()[QStringLiteral("content")].toString());
        if (hasTemplate(title, content)) {
            continue;
        }
        Database::instance().addTemplate(title, content);
    }
}

bool TemplatesModel::hasTemplate(const QString &title, const QString &content)
{
    for (auto item : m_templates) {
        if (item->title() == title && item->content() == content) {
            return true;
        }
    }
    return false;
}
