// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#ifndef COMMENT_H
#define COMMENT_H

#include <QObject>
#include <QJsonObject>

class Comment : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QDateTime creationTime READ creationTime CONSTANT)
    Q_PROPERTY(QString creator READ creator CONSTANT)
    Q_PROPERTY(QString text READ text CONSTANT)
    Q_PROPERTY(int attachmentId READ attachmentId CONSTANT)

public:
    explicit Comment(const QJsonObject &json);

    QString id() const { return m_id; };
    QDateTime creationTime() const
    {
        return m_creationTime;
    };
    QString creator() const { return m_creator; };
    QString text() const { return m_text; }
    int attachmentId() const { return m_attachmentId; };

private:
    QString m_id;
    QDateTime m_creationTime;
    QString m_creator;
    QString m_text;
    int m_attachmentId;

};

#endif // COMMENT_H
