// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include "bugmodel.h"

#include <KLocalizedString>

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QSqlQuery>

#include "database.h"
#include "ladybirdconfig.h"

BugModel::BugModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_errorString(QLatin1String(""))
{
}

void BugModel::addBug(Bug *bug)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_bugs << bug;
    endInsertRows();
}

int BugModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_bugs.count();
}

QVariant BugModel::data(const QModelIndex & index, int role) const {
    if (role != 0 ||  index.row() >= m_bugs.length()) {
        return QVariant();
    }

    return QVariant::fromValue(m_bugs[index.row()]);
}

QHash<int, QByteArray> BugModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[0] = "bug";
    return roles;
}

void BugModel::fetch()
{
    setRefreshing(true);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    manager->setStrictTransportSecurityEnabled(true);
    manager->enableStrictTransportSecurityStore(true);

    // https://bugzilla.readthedocs.io/en/5.0/api/core/v1/bug.html#search-bugs
    QStringList status = {
        QStringLiteral("UNCONFIRMED"),
        QStringLiteral("CONFIRMED"),
        QStringLiteral("ASSIGNED"),
        QStringLiteral("REOPENED"),
        QStringLiteral("NEEDSINFO")
    };
    QString url = QStringLiteral("%1/rest/bug?product=%2&bug_status=%3").arg(LadybirdConfig::baseUrl(), LadybirdConfig::product(), status.join(QStringLiteral("&bug_status=")));

    QNetworkRequest request((QUrl(url)));
    QNetworkReply *reply = manager->get(request);
    connect(reply, &QNetworkReply::finished, this, [this, url, reply]() {
        setRefreshing(false);
        if (reply->error()) {
            qWarning() << "Error fetching feed";
            qWarning() << reply->errorString();
            m_errorString = reply->errorString();
            Q_EMIT errorStringChanged(m_errorString);
            //Q_EMIT error(url, reply->error(), reply->errorString());
        } else {
            m_errorString.clear();
            Q_EMIT errorStringChanged(m_errorString);
            QByteArray data = reply->readAll();
            processResult(data);
        }
        delete reply;
    });
}

void BugModel::processResult(QByteArray &data)
{
    beginResetModel();

    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray array = doc[QStringLiteral("bugs")].toArray();

    m_bugs.clear();

    for (auto item : array) {
        addBug(new Bug(item.toObject()));
    }

    endResetModel();
}

void BugModel::setRefreshing(bool refreshing)
{
    m_refreshing = refreshing;
    Q_EMIT refreshingChanged(m_refreshing);
}

QStringList BugModel::getFlagsForFilter()
{
    QStringList flags;

    flags.append(i18n("All"));

    for (auto bug : m_bugs) {
        flags.append(bug->flags());
    }

    flags.removeDuplicates();
    return flags;
}
