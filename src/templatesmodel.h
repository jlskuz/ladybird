// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#ifndef TEMPLATESMODEL_H
#define TEMPLATESMODEL_H

#include <QAbstractListModel>
#include <QObject>

#include "template.h"

class TemplatesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    TemplatesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex & parent = QModelIndex()) const override;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

    QJsonArray toJson();
    Q_INVOKABLE void exportToFile(const QString &path);
    Q_INVOKABLE void importFromFile(const QString &path);
    void importJson(QJsonArray array);
    bool hasTemplate(const QString &title, const QString &content);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QVector<Template *> m_templates;

    void loadFromDatabase();
};

#endif // TEMPLATESMODEL_H
