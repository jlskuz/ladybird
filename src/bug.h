// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#ifndef BUG_H
#define BUG_H

#include <QObject>
#include "comment.h"

class Bug : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString summary READ summary CONSTANT)
    Q_PROPERTY(QString severity READ severity CONSTANT)
    Q_PROPERTY(QString platform READ platform CONSTANT)
    Q_PROPERTY(QString os READ os CONSTANT)
    Q_PROPERTY(QString version READ version CONSTANT)
    Q_PROPERTY(QDateTime lastUpdate READ lastUpdate CONSTANT)
    Q_PROPERTY(QDateTime creationTime READ creationTime CONSTANT)
    Q_PROPERTY(QStringList flags READ flags CONSTANT)
    Q_PROPERTY(QString commentDraft READ commentDraft WRITE setCommentDraft NOTIFY commentDraftChanged)
    Q_PROPERTY(int statusDraft READ statusDraft WRITE setStatusDraft NOTIFY statusDraftChanged)
    Q_PROPERTY(QVector<Comment *> comments READ comments NOTIFY commentsChanged)
    Q_PROPERTY(bool seen READ seen WRITE setSeen NOTIFY seenChanged)
    Q_PROPERTY(QString errorString READ errorString NOTIFY errorStringChanged)

public:
    Bug(const QJsonObject &json);
    ~Bug();

    QString id() const { return m_id; };
    QString status() const { return m_status; };
    QString summary() const { return m_summary; };
    QString severity() const { return m_severity; };
    QString platform() const { return m_platform; };
    QString os() const { return m_os; };
    QString version() const { return m_version; };
    QStringList flags() const { return m_flags; };
    QDateTime lastUpdate() const
    {
        return m_lastUpdate;
    };
    QDateTime creationTime() const
    {
        return m_creationTime;
    };
    QString commentDraft() const { return m_commentDraft; };
    int statusDraft() const { return m_statusDraft; };
    QVector<Comment *> comments() const { return m_comments; };
    bool seen() const { return m_seen; };
    QString errorString() const { return m_errorString; };


    void setSeen(bool seen);
    void setCommentDraft(const QString &commentDraft);
    void setStatusDraft(int statusDraft);

    Q_INVOKABLE void copyId();
    Q_INVOKABLE void fetchComments(bool force = false);
    Q_INVOKABLE void postComment(const QString &comment, int status = 0, QString dupId = QString());

Q_SIGNALS:
    void statusChanged(const QString &status);
    void commentDraftChanged(const QString &status);
    void statusDraftChanged(const QString &status);
    void commentsChanged(QVector<Comment *>);
    void seenChanged(bool seen);
    void errorStringChanged(const QString &errorString);
    void postCommentDone(const QString &errorString = {});

private:
    QString m_id;
    QString m_status;
    QString m_summary;
    QString m_severity;
    QString m_platform;
    QString m_os;
    QString m_version;
    QDateTime m_lastUpdate;
    QDateTime m_creationTime;
    QString m_commentDraft;
    QStringList m_flags;
    int m_statusDraft;
    QVector<Comment *> m_comments;
    bool m_seen;

    QString m_errorString;

    void processCommentResult(QByteArray &dat);
    void update(QByteArray &data);
};

#endif // BUG_H
