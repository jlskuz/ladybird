// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#ifndef BUGSPROXYMODEL_H
#define BUGSPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QObject>

class BugsProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(QString flagsFilter READ flagsFilter WRITE setFlagsFilter NOTIFY flagsFilterChanged)
    Q_PROPERTY(int flagsOperator READ flagsOperator WRITE setFlagsOperator NOTIFY flagsOperatorChanged)
    Q_PROPERTY(QString osFilter READ osFilter WRITE setOsFilter NOTIFY osFilterChanged)
    Q_PROPERTY(int seenFilter READ seenFilter WRITE setSeenFilter NOTIFY seenFilterChanged)
    Q_PROPERTY(QString severityFilter READ severityFilter WRITE setSeverityFilter NOTIFY severityFilterChanged)
    Q_PROPERTY(QString statusFilter READ statusFilter WRITE setStatusFilter NOTIFY statusFilterChanged)
    Q_PROPERTY(QString searchFilter READ searchFilter WRITE setSearchFilter NOTIFY searchFilterChanged)
    Q_PROPERTY(QString versionFilter READ versionFilter WRITE setVersionFilter NOTIFY versionFilterChanged)

public:
    enum BugsSeenFilter {
        ALL = 0,
        UNSEEN,
        SEEN
    };

    explicit BugsProxyModel(QObject *parent = nullptr);
    ~BugsProxyModel() override;

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

    QString flagsFilter() const { return m_flagsFilter; };
    void setFlagsFilter(const QString &filter);
    int flagsOperator() const { return m_flagsOperator; };
    void setFlagsOperator(int value);
    QString osFilter() const { return m_osFilter; };
    void setOsFilter(const QString &filter);
    int seenFilter() const { return m_seenFilter; };
    void setSeenFilter(int filter);
    QString severityFilter() const { return m_severityFilter; };
    void setSeverityFilter(const QString &filter);
    QString statusFilter() const { return m_statusFilter; };
    void setStatusFilter(const QString &filter);
    QString searchFilter() const { return m_searchFilter; };
    void setSearchFilter(const QString &filter);
    QString versionFilter() const { return m_versionFilter; };
    void setVersionFilter(const QString &filter);

Q_SIGNALS:
    void flagsFilterChanged();
    void flagsOperatorChanged();
    void osFilterChanged();
    void versionFilterChanged();
    void seenFilterChanged();
    void severityFilterChanged();
    void statusFilterChanged();
    void searchFilterChanged();

private:
    QString m_flagsFilter;
    int m_flagsOperator;
    QString m_osFilter;
    BugsSeenFilter m_seenFilter;
    QString m_severityFilter;
    QString m_statusFilter;
    QString m_searchFilter;
    QString m_versionFilter;
};

#endif // BUGSPROXYMODEL_H
